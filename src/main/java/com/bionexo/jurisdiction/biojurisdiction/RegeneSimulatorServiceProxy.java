package com.bionexo.jurisdiction.biojurisdiction;

import com.bionexo.jurisdiction.biojurisdiction.beans.JurisdictionQuoteBean;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

//@FeignClient(name="regene-simulator", url="localhost:8000")
//@FeignClient(name="regene-simulator")
@FeignClient(name = "zuul-api-gateway-server")
@RibbonClient(name="regene-simulator")
public interface RegeneSimulatorServiceProxy {

    //@GetMapping("/quote/request/{quoteId}")
    @GetMapping("/regene-simulator/quote/request/{quoteId}")
    public JurisdictionQuoteBean retrieveQuoteValue(@PathVariable("quoteId") String quoteId);
}
