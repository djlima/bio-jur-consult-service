package com.bionexo.jurisdiction.biojurisdiction.models;

import java.util.Date;

public class JurisdictionQuote {

    private Integer quoteId;
    private Integer companyId;
    private Integer requesterUserId;
    private Integer replierUserId;
    private String quoteStatus;
    private Date createdAt;
    private Date updatedAt;

}
