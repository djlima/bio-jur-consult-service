package com.bionexo.jurisdiction.biojurisdiction.models;

import java.util.Date;

public class JurisdictionQuoteActions {

    private Integer id;
    private Integer quoteId;
    private Integer requesterUserId;
    private Integer repplierUserId;
    private Date generatedAt;
    private String action;
    private String reason;
}
