package com.bionexo.jurisdiction.biojurisdiction.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;

@Entity
public class JurisdictionApprovation {

    @Id
    @GeneratedValue
    private Integer id;

    private Integer userId;
    private Integer productCategoryId;
    private Integer jurisdictionApproverId;
    private Integer validDays;
    private Boolean isBySupplier;
    private Double maxValue;
    private Date createdAt;
    private Date updatedAt;
    private Date renewsAt;


}
