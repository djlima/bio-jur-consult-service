package com.bionexo.jurisdiction.biojurisdiction;

import brave.sampler.Sampler;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@EnableFeignClients("com.bionexo.jurisdiction.biojurisdiction")
@EnableDiscoveryClient
@EnableHystrix
public class BioJurisdictionApplication {

	public static void main(String[] args) {
		SpringApplication.run(BioJurisdictionApplication.class, args);
	}

	@Bean
	public Sampler defaultSamples() {
		return Sampler.ALWAYS_SAMPLE;
	}


}
