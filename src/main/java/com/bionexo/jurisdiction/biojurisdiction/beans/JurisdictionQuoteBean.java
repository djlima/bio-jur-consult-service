package com.bionexo.jurisdiction.biojurisdiction.beans;

import java.math.BigDecimal;

public class JurisdictionQuoteBean {
    private Long id;
    private String quoteId;
    private BigDecimal total;
    private int port;

    public JurisdictionQuoteBean() {
    }

    public JurisdictionQuoteBean(Long id, String quoteId, BigDecimal total, int port) {
        this.id = id;
        this.quoteId = quoteId;
        this.total = total;
        this.port = port;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getQuoteId() {
        return quoteId;
    }

    public void setQuoteId(String quoteId) {
        this.quoteId = quoteId;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }
}
