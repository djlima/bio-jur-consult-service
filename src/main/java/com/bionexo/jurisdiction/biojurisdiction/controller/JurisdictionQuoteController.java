package com.bionexo.jurisdiction.biojurisdiction.controller;

import com.bionexo.jurisdiction.biojurisdiction.RegeneSimulatorServiceProxy;
import com.bionexo.jurisdiction.biojurisdiction.beans.JurisdictionQuoteBean;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

@RestController
public class JurisdictionQuoteController {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private RegeneSimulatorServiceProxy proxy;

    @GetMapping("/validate/quote/{quoteId}/{total}")
    public JurisdictionQuoteBean validateQuote(@PathVariable String quoteId,@PathVariable BigDecimal total) {

        Map<String, String> uriVariables = new HashMap<>();
        uriVariables.put("quoteId", quoteId);
        ResponseEntity<JurisdictionQuoteBean> responseEntity = new RestTemplate()
                .getForEntity("http://localhost:8000/quote/request/{quoteId}", JurisdictionQuoteBean.class, uriVariables);

        JurisdictionQuoteBean response = responseEntity.getBody();

        return new JurisdictionQuoteBean(response.getId(), response.getQuoteId(), response.getTotal(), response.getPort());
    }

    @GetMapping("/validate/quote-feign/{quoteId}/{total}")
    public JurisdictionQuoteBean validateQuoteFeign(@PathVariable String quoteId,@PathVariable BigDecimal total) {

        JurisdictionQuoteBean response = proxy.retrieveQuoteValue(quoteId);

        logger.info("{}", response);

        return new JurisdictionQuoteBean(response.getId(), response.getQuoteId(), response.getTotal(), response.getPort());
    }

    @GetMapping("/fault-tolerance-example")
    @HystrixCommand(fallbackMethod = "fallbackRetrieveConfiguration")
    public JurisdictionQuoteBean retrieveConfiguration() {
        throw new RuntimeException("Not available");
    }

    public JurisdictionQuoteBean fallbackRetrieveConfiguration() {
        return new JurisdictionQuoteBean(1L,"1",BigDecimal.valueOf(1),1);
    }
}
