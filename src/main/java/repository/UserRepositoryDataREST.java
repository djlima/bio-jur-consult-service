package repository;

import com.bionexo.jurisdiction.biojurisdiction.user.Post;
import com.bionexo.jurisdiction.biojurisdiction.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

@RestResource(path="users2", rel="users")
public interface UserRepositoryDataREST extends JpaRepository<User, Integer> {
}
